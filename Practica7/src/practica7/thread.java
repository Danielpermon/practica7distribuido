package practica7;

import java.util.logging.Level;
import java.util.logging.Logger;

public class thread extends Thread{
    
    Operaciones bloque;
    int b;
    int myID;
    boolean busy;
    static Lock lock;
    
    public thread(Lock lock, int id){
        this.lock = lock;
        myID=id;
        busy=false;
    }
    
    public void setBloque(Operaciones aux, int c){
        bloque = aux;
        b=c;
    }
    
    public void CriticalRegion() throws InterruptedException {
        if(bloque.getCounter()>0&&!bloque.ocupado(b)){
            System.out.println("soy el hilo "+this.getId()+" con el bloque "+(b+1)+" y estoy en la región crítica");
            busy=true;
        }
        Thread.sleep(3000);
    }
    
    public void asignaBloque(Operaciones aux){
        bloque=aux;
    }
    
    public boolean bloque(){
        if(bloque==null)
            return false;
        else return true;
    }

    public void nonCriticalRegion() throws InterruptedException {
        if (bloque.getCounter() > 0 && busy) {
            
            switch (bloque.getOperacion()) {
                case "+":
                    bloque.suma(b);
                    break;
                case "-":
                    bloque.resta(b);
                    break;
                case "*":
                    bloque.multi(b);
                    break;
                case "#":
                    bloque.combinacion(String.valueOf(bloque.getAlfa()), String.valueOf(1 - bloque.getAlfa()), b);
                    break;
            }
            bloque.updateCounter();
        }
        System.out.println("soy el hilo "+this.getId()+" con el bloque "+(b+1)+" y no estoy en la región crítica");
        Thread.sleep(3000);
    }
    
    @Override
    public void run(){
        while (bloque.getCounter()>0) {
            lock.requestCR(myID);
            try {
                CriticalRegion();
            } catch (InterruptedException ex) {
                Logger.getLogger(thread.class.getName()).log(Level.SEVERE, null, ex);
            }
            lock.releaseCR(myID);
            try {
                nonCriticalRegion();
            } catch (InterruptedException ex) {
                Logger.getLogger(thread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
