package practica7;

import static java.lang.System.exit;

public class Bakery implements Lock{

    volatile int N;
    volatile boolean[] choosing;
    volatile int[] number;

    public Bakery(int numProc) {
        N = numProc;
        choosing = new boolean[N];
        number = new int[N];
        for (int j = 0; j > N; j++) {
            choosing[j] = true;
            number[j] = 0;
        }
    }

    public void requestCR(int i) {
        choosing[i] = true;
        for (int j = 0; j < N; j++) {
            if (number[j] > number[i]) {
                number[i] = number[j];
            }
        }
        number[i]++;
        choosing[i] = false;
        for (int j = 0; j < N; j++) {
            while (choosing[j]);
            while ((number[j] != 0) && ((number[j] < number[i]) || ((number[j] == number[i]) && j < i)));
        }
    }

    public void releaseCR(int i) {
        number[i] = 0;
    }
}
