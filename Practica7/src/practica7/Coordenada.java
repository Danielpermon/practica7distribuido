package practica7;

public class Coordenada {
    
    private int row;
    private int column;

    public Coordenada(int fila, int col) {
        this.row = fila;
        this.column = col;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int fil) {
        this.row = fil;
    }

}
