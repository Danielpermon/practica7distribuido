/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica7;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;
import practica7.Operaciones;
import practica7.Pixeles;

public class Inicial extends javax.swing.JFrame {

    BufferedImage result;
    File selectedImage1,selectedImage2;
    private Pixeles[][] im1;
    private Pixeles[][] im2;
    private Pixeles[][] imR;
    private Operaciones op;
    
    public Inicial() {
        initComponents();
        result = null;
    }
    
    public int[][] pixel2matrix(Pixeles[][] pixels) {
        int[][] matrix = new int[pixels.length][pixels[0].length];
        for (int i = 0; i < pixels.length; i++) {
            for (int j = 0; j < pixels[i].length; j++) {
                int alpha = (int) pixels[i][j].getAlpha();
                int red = (int) pixels[i][j].getRed();
                int green = (int) pixels[i][j].getGreen();
                int blue = (int) pixels[i][j].getBlue();
                int p = (alpha << 24) | (red << 16) | (green << 8) | blue;
                matrix[i][j] = p;
            }
        }
        return matrix;
    }

    public Pixeles[][] matrix2pixel(int[][] matrix) {
        Pixeles[][] pixels = new Pixeles[matrix.length][matrix[0].length];

        for (int i = 0; i < pixels.length; i++) {
            for (int j = 0; j < pixels[i].length; j++) {
                int alpha = (int) matrix[i][j] >> 24;
                int red = ((int) matrix[i][j] << 8) >> 24;
                int green = ((int) matrix[i][j] << 16) >> 24;
                int blue = ((int) matrix[i][j] << 24) >> 24;
                Pixeles p = new Pixeles((byte) alpha, (byte) red, (byte) green, (byte) blue);
                pixels[i][j] = p;
            }
        }
        return pixels;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        img2 = new javax.swing.JLabel();
        img1 = new javax.swing.JLabel();
        btnIm1 = new javax.swing.JButton();
        btnIm2 = new javax.swing.JButton();
        operaciones = new javax.swing.JComboBox();
        btnOperar = new javax.swing.JButton();
        txtIm1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtFilas = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtColumnas = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        img2.setBackground(new java.awt.Color(255, 255, 255));

        img1.setBackground(new java.awt.Color(255, 255, 255));

        btnIm1.setText("Cargar Img1");
        btnIm1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIm1ActionPerformed(evt);
            }
        });

        btnIm2.setText("Cargar Img2");
        btnIm2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIm2ActionPerformed(evt);
            }
        });

        operaciones.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "+", "-", "*", "#" }));

        btnOperar.setText("Operar");
        btnOperar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOperarActionPerformed(evt);
            }
        });

        txtIm1.setText("0.");

        jLabel1.setText("Peso");

        txtFilas.setText("0");

        jLabel3.setText("Filas");

        txtColumnas.setText("0");

        jLabel4.setText("Columnas");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(171, 171, 171)
                        .addComponent(btnIm1)
                        .addGap(279, 279, 279)
                        .addComponent(btnIm2))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(img1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(169, 169, 169)
                                .addComponent(jLabel1)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(txtIm1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(84, 84, 84)
                                        .addComponent(img2, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(28, 28, 28)
                                        .addComponent(operaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(txtFilas, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4)
                                    .addComponent(txtColumnas, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnOperar))))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtIm1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(img1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtFilas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel4)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtColumnas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnOperar))
                                .addComponent(img2, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(139, 139, 139)
                        .addComponent(operaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnIm1)
                    .addComponent(btnIm2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIm1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIm1ActionPerformed
        JFileChooser file = new JFileChooser();
        file.setCurrentDirectory(new File("C:\\Users\\Daniel Pérez\\Pictures"));
        //filtra los tipos de archivo permitidos
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
        file.addChoosableFileFilter(filter);
        int result = file.showSaveDialog(null);
        //se verifica que el usuario haya ingresado una imagen
        if (result == JFileChooser.APPROVE_OPTION) {
            selectedImage1 = file.getSelectedFile();
            String path = selectedImage1.getAbsolutePath();
            img1.setIcon(ResizeImage(path, img1));
        } else if (result == JFileChooser.CANCEL_OPTION) {
            System.out.println("No File Select");
        }
    }//GEN-LAST:event_btnIm1ActionPerformed

    private void btnIm2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIm2ActionPerformed
        JFileChooser file = new JFileChooser();
        file.setCurrentDirectory(new File("C:\\Users\\Daniel Pérez\\Pictures"));
        //filter the files
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
        file.addChoosableFileFilter(filter);
        int result = file.showSaveDialog(null);
        //if the user click on save in Jfilechooser
        if (result == JFileChooser.APPROVE_OPTION) {
            selectedImage2 = file.getSelectedFile();
            String path = selectedImage2.getAbsolutePath();
            img2.setIcon(ResizeImage(path, img2));
        } else if (result == JFileChooser.CANCEL_OPTION) {
            System.out.println("No File Select");
        }
    }//GEN-LAST:event_btnIm2ActionPerformed

    private void btnOperarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOperarActionPerformed
        
        String opcion = operaciones.getSelectedItem().toString();
        try{
            BufferedImage image1 = ImageIO.read(selectedImage1);
            BufferedImage image2 = ImageIO.read(selectedImage2);
            result = ImageIO.read(selectedImage1);
            
            im1 = new Pixeles[image1.getWidth()][image1.getHeight()];
            im2 = new Pixeles[image2.getWidth()][image2.getHeight()];
            
            for(int i=0;i<image1.getWidth();i++){
                for(int j=0;j<image1.getHeight();j++){
                    Color c = new Color(image1.getRGB(i, j));
                    im1[i][j]=new Pixeles((byte)c.getAlpha(),(byte)c.getRed(),(byte)c.getGreen(),(byte)c.getBlue());
                }
            }
            for(int i=0;i<image2.getWidth();i++){
                for(int j=0;j<image2.getHeight();j++){

                    Color c = new Color(image2.getRGB(i, j));
                    im2[i][j]=new Pixeles((byte)c.getAlpha(),(byte)c.getRed(),(byte)c.getGreen(),(byte)c.getBlue());
                }
            }
        }
        
        catch (IOException e1) {
            e1.printStackTrace();
	}
        
        op= new Operaciones(txtFilas.getText(),txtColumnas.getText(),
                Integer.parseInt(txtColumnas.getText())*Integer.parseInt(txtFilas.getText()),
                selectedImage1, selectedImage2,opcion,txtIm1.getText());
     
        try {
            op.calcula();
        } catch (InterruptedException ex) {
            Logger.getLogger(Inicial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnOperarActionPerformed

    public ImageIcon ResizeImage(String ImagePath, JLabel label){
        ImageIcon MyImage = new ImageIcon(ImagePath);
        Image img = MyImage.getImage();
        Image newImg = img.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        return image;
    }
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inicial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIm1;
    private javax.swing.JButton btnIm2;
    private javax.swing.JButton btnOperar;
    private javax.swing.JLabel img1;
    private javax.swing.JLabel img2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JComboBox operaciones;
    private javax.swing.JTextField txtColumnas;
    private javax.swing.JTextField txtFilas;
    private javax.swing.JTextField txtIm1;
    // End of variables declaration//GEN-END:variables
}
