package practica7;

public class Chunk {
    
    private Coordenada cpos;
    private int crows;
    private int ccols;
    
    public Chunk(int row, int col, int rows, int cols) {
        this.cpos = new Coordenada(row, col);
        this.crows = rows;
        this.ccols = cols;
    }

    public int getCcols() {
        return ccols;
    }

    public int getCrows() {
        return crows;
    }

    public int getX(){
        return cpos.getColumn();
    }
    
    public int getY(){
        return cpos.getRow();
    }
}
