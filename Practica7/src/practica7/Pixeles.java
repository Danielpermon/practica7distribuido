package practica7;

public class Pixeles {
    private byte alfa;
    private byte rojo;
    private byte verde;
    private byte azul;
    
    public Pixeles(byte a, byte r, byte g, byte b){
        alfa=a;
        rojo=r;
        verde=g;
        azul=b;
    }
    
    public byte getAlpha() {
        return alfa;
    }

    public byte getBlue() {
        return azul;
    }

    public byte getGreen() {
        return verde;
    }

    public byte getRed() {
        return rojo;
    }
}
