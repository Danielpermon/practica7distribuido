package practica7;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Operaciones {
    
    private int yMax;
    private int xMax;  
    private int chunkrows;
    private int chunkcols;
    private int chunkCounter;
    private File im;
    private File im2;
    private Chunk matrix[];
    private BufferedImage bufferedImage1 = null;
    private BufferedImage bufferedImage2 = null;
    private BufferedImage result = null;
    private boolean chunkMatrix [];
    private boolean busy;
    private thread hilos[];
    JFrame frame;
    
    private static Lock lock;
    
    private double alfa,beta;
    
    private String operacion;
    
    public Operaciones(String rows, String cols, int bloques, File uno, File dos, String op, String peso){
        
        if(rows == "") chunkrows=0; else chunkrows=Integer.parseInt(rows);
        
        if(cols == "") chunkcols=0; else chunkcols=Integer.parseInt(cols);
        chunkCounter=bloques;
        operacion = op;
        im=uno;
        im2=dos;
        
        frame = new JFrame();
        
        alfa = Double.parseDouble(peso);
        beta = 1-alfa;
        
        try{
            bufferedImage1 = ImageIO.read(im);
            bufferedImage2 = ImageIO.read(im2);
        }catch (IOException e1) {
            e1.printStackTrace();
        }
        
        if(bufferedImage1.getHeight()>bufferedImage2.getHeight()) yMax=bufferedImage2.getHeight(); else yMax=bufferedImage1.getHeight();
        if(bufferedImage1.getWidth()>bufferedImage2.getWidth()) xMax=bufferedImage2.getWidth(); else xMax=bufferedImage1.getWidth();
        
        chunkMatrix = new boolean[chunkCounter];
        matrix = new Chunk[chunkCounter];
        hilos = new thread[chunkCounter];
        int contF = 0;
        int contC = 0;
        
        for(int i = 0;i<chunkCounter;i++){
            if(i==0){
                matrix[i] = new Chunk(0,0,yMax/chunkrows,xMax/chunkcols);
                contF++;
            }
            else{
                if(contF<chunkrows && contC==0){
                    matrix[i] = new Chunk((contF*yMax/chunkrows)+1,(contC*xMax/chunkcols),yMax/chunkrows,xMax/chunkcols);
                    contF++;
                }
                else{
                    if(contF<chunkrows){
                        matrix[i] = new Chunk((contF*yMax/chunkrows)+1,(contC*xMax/chunkcols)+1,yMax/chunkrows,xMax/chunkcols);
                        contF++;
                    }
                    else{
                        contC++;
                        contF = 0;
                        matrix[i] = new Chunk((contF*yMax/chunkrows),(contC*xMax/chunkcols)+1,yMax/chunkrows,xMax/chunkcols);
                        contF++;
                    }
                }
                
            }            
            chunkMatrix[i]=false;
        }
        
        File f = null;
        
        try{
            f = new File("C:\\Users\\Daniel Pérez\\Pictures\\negro.jpg");
            result = new BufferedImage(xMax, yMax, BufferedImage.TYPE_INT_ARGB);
            result = ImageIO.read(f);
            
            result = resize(result,xMax,yMax);
            
        }catch(IOException e){
            System.out.println("Error: "+e);
        }
    }
    
    public boolean ocupado(int x){
        if(!chunkMatrix[x]){
            chunkMatrix[x]=true;
            return true;
        }
        else return false;
    }
    
    public void updateCounter(){
        chunkCounter--;
    }
    
    public int getCounter(){
        return chunkCounter;
    }
    
    public void calcula() throws InterruptedException {
        
        lock = new Bakery(chunkCounter);
        
//        int cont=0;
//        while(chunkCounter>0){
//            System.out.println(chunkCounter);
//            hilos[cont] = new thread(lock, cont);
//            hilos[cont].start();
//            hilos[cont].setBloque(this, cont);
//        }
        
        for (int j = 0; j < chunkCounter; j++) {
            System.out.println(chunkCounter+" "+j);
            hilos[j] = new thread(lock, j);
            hilos[j].start();
            hilos[j].setBloque(this, j);
            try{
                hilos[j].join();
            }catch(Exception e){}
        }

    }
    
    public BufferedImage getResultado(){
        return result;
    }
    
    public void suma(int x) {
        for (int i = matrix[x].getX(); i < (matrix[x].getX() + (xMax / chunkcols) - 1); i++) {
            for (int j = matrix[x].getY(); j < (matrix[x].getY() + (yMax / chunkrows) - 1); j++) {
                Color c = new Color(bufferedImage1.getRGB(i, j));
                Color c2 = new Color(bufferedImage2.getRGB(i, j));
                int a = (c.getAlpha() + c2.getAlpha()) / 2;
                int r = (c.getRed() + c2.getRed()) / 2;
                int g = (c.getGreen() + c2.getGreen()) / 2;
                int b = (c.getBlue() + c2.getBlue()) / 2;
                int p = (a << 24) | (r << 16) | (g << 8) | b;
                result.setRGB(i, j, p);
            }
        }
        Pinta(result,x);
    }
    
    public void resta(int x){
        for (int i = matrix[x].getX(); i < (matrix[x].getX() + (xMax / chunkcols) - 1); i++) {
            for (int j = matrix[x].getY(); j < (matrix[x].getY() + (yMax / chunkrows) - 1); j++) {
                Color c = new Color(bufferedImage1.getRGB(i, j));
                Color c2 = new Color(bufferedImage2.getRGB(i, j));
                int a = 0;
                int r = 0;
                int g = 0;
                int b = 0;

                if (c.getAlpha() - c2.getAlpha() >= 0) {
                    a = c.getAlpha() - c2.getAlpha();
                }

                if (c.getRed() - c2.getRed() >= 0) {
                    r = c.getRed() - c2.getRed();
                }

                if (c.getGreen() - c2.getGreen() >= 0) {
                    g = c.getGreen() - c2.getGreen();
                }

                if (c.getBlue() - c2.getBlue() >= 0) {
                    b = c.getBlue() - c2.getBlue();
                }

                int p = (a << 24) | (r << 16) | (g << 8) | b;
                result.setRGB(i, j, p);
            }
        }
        Pinta(result,x);
    }
    
    public void multi(int x){
        for (int i = matrix[x].getX(); i < (matrix[x].getX() + (xMax / chunkcols) - 1); i++) {
            for (int j = matrix[x].getY(); j < (matrix[x].getY() + (yMax / chunkrows) - 1); j++) {
                Color c = new Color(bufferedImage1.getRGB(i, j));
                Color c2 = new Color(bufferedImage2.getRGB(i, j));
                int a = (int) (c.getAlpha() * c2.getAlpha()) / 255;
                int r = (int) (c.getRed() * c2.getRed()) / 255;
                int g = (int) (c.getGreen() * c2.getGreen()) / 255;
                int b = (int) (c.getBlue() * c2.getBlue()) / 255;
                int p = ((a << 24) | (r << 16) | (g << 8) | b);
                result.setRGB(i, j, p);
            }
        }    
        Pinta(result,x);
    }    
    
    public void combinacion(String p1, String p2,int x){
        alfa = Double.parseDouble(p1);
        beta = Double.parseDouble(p2);
        for (int i = matrix[x].getX(); i < (matrix[x].getX() + (xMax / chunkcols) - 1); i++) {
            for (int j = matrix[x].getY(); j < (matrix[x].getY() + (yMax / chunkrows) - 1); j++) {
                Color c = new Color(bufferedImage1.getRGB(i, j));
                Color c2 = new Color(bufferedImage2.getRGB(i, j));
                int a = (int) ((c.getAlpha() * alfa) + (c2.getAlpha() * beta));
                int r = (int) ((c.getRed() * alfa) + (c2.getRed() * beta));
                int g = (int) ((c.getGreen() * alfa) + (c2.getGreen() * beta));
                int b = (int) ((c.getBlue() * alfa) + (c2.getBlue() * beta));
                int p = (a << 24) | (r << 16) | (g << 8) | b;
                result.setRGB(i, j, p);
            }
        }
        Pinta(result,x);
    }
    
    public Chunk obtainChunk(int n){
        Chunk x = null;
        for(int i = 0;i<chunkCounter;i++){
            if(i==n)
                x=matrix[i];
        }
        
        return x;
    }
    
    public double getAlfa(){return alfa;}
    
    public void Pinta(BufferedImage f, int i){
        frame.getContentPane().setLayout(new FlowLayout());
        //frame.getContentPane().add(new JLabel(new ImageIcon(f)));
        frame.setContentPane(new JLabel(new ImageIcon(f)));
        frame.setVisible(true);
        
        try{
            ImageIO.write(f, "jpg", new File("C://Users/Daniel Pérez/Pictures/resultado"+String.valueOf(i)+".jpg"));
        }catch(IOException e){
            System.out.println("Error: "+e);
        }
        System.out.println("Done");
    }
    
    public static BufferedImage resize(BufferedImage bufferedImage, int newW, int newH) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        BufferedImage bufim = new BufferedImage(newW, newH, bufferedImage.getType());
        Graphics2D g = bufim.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(bufferedImage, 0, 0, newW, newH, 0, 0, w, h, null);
        g.dispose();
        return bufim;
    }

    public String getOperacion(){
        return operacion;
    }
}
